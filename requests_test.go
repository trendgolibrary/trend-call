package requests

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/labstack/echo/v4"
)

func TestCallGetQuery(t *testing.T) {
	param := Params{
		URL: "https://petstore.swagger.io/v2/pet/findByStatus",
		QUERY: map[string]string{
			"status": "available",
		},
		HEADERS: map[string]string{
			echo.HeaderContentType: "application/json",
		},
		TIMEOUT: 5,
	}
	var res Response
	if err := Call().Get(param, &res).Error(); err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(res.Result))
}

func TestCallGetParams(t *testing.T) {
	param := Params{
		URL: "https://petstore.swagger.io/v2/pet/:petId",
		PARAMS: map[string]string{
			"petId": "9223372036854114357",
		},
		HEADERS: map[string]string{
			echo.HeaderContentType: "application/json",
		},
		TIMEOUT: 5,
	}
	var res Response
	if err := Call().Get(param, &res).Error(); err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(res.Result))
}

func TestCallPost(t *testing.T) {
	str := `{
		"id": 0,
		"category": {
			"id": 0,
			"name": "string"
		},
		"name": "doggie",
		"photoUrls": [
			"string"
		],
		"tags": [
			{
				"id": 0,
				"name": "string"
			}
		],
		"status": "available"
	}`

	param := Params{
		URL:  "https://petstore.swagger.io/v2/pet",
		BODY: bytes.NewBuffer([]byte(str)),
		HEADERS: map[string]string{
			echo.HeaderContentType: "application/json",
		},
		TIMEOUT: 5,
	}
	var res Response
	if err := Call().Post(param, &res).Error(); err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(res.Result))
}

func TestMapQueryParams(t *testing.T) {
	t.Run(`OK`, func(t *testing.T) {
		params := Params{
			URL: `http://localhost:3000/api/v1/:register/email`,
			QUERY: map[string]string{
				"email": "defida5197@runchet.com",
			},
			PARAMS: map[string]string{
				"register": "123",
			},
		}

		println(mapParamsQueryToURL(params))

	})
}
